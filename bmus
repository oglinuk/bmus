#!/bin/bash

# Check if BMUSDIR env is set; if not default to $HOME/Documents/bmus
# Check if bmus directory exists; if not prompt to create it
# Check if $target exists
# rsync $target to bmus directory
# Compare diff $target in bmus directory to original; if failed retry
# Compress $target (TODO: add different compression options)
# Add && commit changes
# Encrypt with AES-256?

target=${*%/}
base=$(basename $target)
declare -i retried=0

compress() {
	cd $BMUSDIR
	tar czf "$base.tar.gz" $base
	rm -rf $base
}

verifydiff() {
	if [ $(diff -r "$BMUSDIR/$base" $target) ]; then
		echo "Found something different with BMUS backup ..."
		return 1
	fi
	echo "Diff verification was a success ..."
	return 0
}

add() {
	echo "Scotty is backing up $target ..."
	rsync -azz $target $BMUSDIR
	if ! verifydiff; then
		if (( $retried < 1 )); then
			retried=$(( retried+1 ))
			echo "Something went wrong with diff verification, retry attempt $retried ..."
			add
		else
			echo "Exceeded max retries with no success ..."
			return 1
		fi
	else
		compress
		git add --all
		git commit -m "Added $base to BMUS directory"
	fi
}

checktarget() {
	if [ ! -f $target ] || [ ! -d $target ]; then
		echo "$target does not seem to exist ..."
		return 1
	fi
}

checkbmus() {
	if [ ! $BMUSDIR ]; then
		echo "BMUSDIR not set, defaulting to $HOME/Documents/bmus ..."
		BMUSDIR="$HOME/Documents/bmus"
	elif [ $(basename $BMUSDIR) != "bmus" ]; then
		BMUSDIR="${BMUSDIR%/}/bmus"
	fi

	if [ ! -d $BMUSDIR ]; then
		declare mkbmus
		read -p "Could not find BMUS directory, create $BMUSDIR now? [y/N]: " mkbmus
		if [[ $mkbmus =~ ^[yY] ]]; then
			mkdir $BMUSDIR && cd $BMUSDIR
			git init
			echo "# (B)ack (M)e (U)p (S)cotty Directory" > README.md
			git add --all
			git commit -m "Initial BMUS commit"
		else
			return 1
		fi
	fi
}

main() {
	checkbmus
	checktarget
	add
}

main
